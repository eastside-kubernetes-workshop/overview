# Project Listing

Provide a brief description of the sub projects and a link to them.

# Workshop Lemonade Demo

https://gitlab.com/eastside-kubernetes-workshop/workshop-lemonade-demo

This is a small demo project uses as the first set of services you can
deploy to your cluster. 

# Service Mutual Authentication 

https://gitlab.com/gauntletwizard/bazel-go/tree/networksecure/network-secure

This is the code for the talk given at the June, 2019 meeting, on June 26.
