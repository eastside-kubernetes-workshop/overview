```
Copyright 2019 Eastside Kubernetes Workshop

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.

See: https://gitlab.com/eastside-kubernetes-workshop/overview/blob/master/LICENSE
```

# Eastside Kubernetes Workshop Overview

The Eastside Kubernetes Workshop is a MeetUp for people in the Bellevue,
Redmond, Issaquah and Kirkland, Washington area to learn about and share
knowledge of using Kubernetes for production workloads. 

It is run as a digital "maker space" where the community supports each other
in building, testing and refining projects in the Kubernetes ecosystem.

The MeetUp URL is here:
https://www.meetup.com/Eastside-Kubernetes-Workshop/

The projects in this group are the source code for the bits and pieces of work
we have tinkered with during our monthly meetings. 

# Project Listing

* Lemanode Demo - https://gitlab.com/eastside-kubernetes-workshop/workshop-lemonade-demo  
This is our first demostration project, a virtual lemonade stand. Start
here https://gitlab.com/eastside-kubernetes-workshop/workshop-lemonade-demo/overview for the 
overview.

* Control Theory for SRE - https://gitlab.com/gauntletwizard/pid-controller-talk  
This talk was incubated and test-driven out the Eastside k8s Workshop. For
the overview see http://pidtalk.kube.gauntletwizard.net/presentation.slide.


# Contributors Welcome

Please feel free to contribute or use these projects for your own Kubernetes
knowlege ramp up. Better yet, start and support your own Kubernetes MeetUp
for the community near you.

Sincerely, Mark P. Hahn,
July 2019.
